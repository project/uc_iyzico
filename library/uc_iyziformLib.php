<?php




class CheckoutFormIyzico
{
	public $_options;
	public function __construct($options) {
		$this->_options = $options;
	}
	
    public function should_initialize_checkout_form($order)
    {
		global $user;
		//var_dump($user); exit();
		$conv_id = rand(100000000,999999999);
		$_SESSION['conversationId'] = $conv_id;
		$country = uc_get_country_data($order->billing_country);
		//echo "<pre>";
		//var_dump($order); exit();
        $request = new \Iyzipay\Request\CreateCheckoutFormInitializeRequest();
		$request->setLocale(\Iyzipay\Model\Locale::TR);
		$request->setConversationId($conv_id);
		$request->setPrice($order->order_total);
		$request->setPaidPrice($order->order_total);
		$request->setCurrency(\Iyzipay\Model\Currency::TL);
		$request->setBasketId($order->order_id);
		$request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
		$request->setCallbackUrl(url('uc_iyzico/callback', array('absolute' => TRUE)));
		$request->setEnabledInstallments(array(1, 2, 3, 6, 9));
		$buyer = new \Iyzipay\Model\Buyer();
		$buyer->setId($order->uid);
		$buyer->setName($order->billing_first_name);
		$buyer->setSurname($order->billing_last_name);
		$buyer->setGsmNumber($order->billing_last_name);
		$buyer->setEmail($order->primary_email);
		$buyer->setIdentityNumber($order->billing_phone);
		$buyer->setLastLoginDate((isset($user->login)) ? date('Y-m-d H:i:s',$user->login) : date('Y-m-d H:i:s',$order->created));
		$buyer->setRegistrationDate((isset($user->access)) ? date('Y-m-d H:i:s',$user->access) : date('Y-m-d H:i:s',$order->created));
		$buyer->setRegistrationAddress($order->billing_street1 . (($order->billing_street2) ? ' ' . $order->billing_street2 : '') . ', ' . $order->billing_city);
		$buyer->setIp($order->host);
		$buyer->setCity(uc_get_zone_code($order->billing_zone));
		$buyer->setCountry($country[0]['country_name']);
		$buyer->setZipCode($order->billing_postal_code);
		$request->setBuyer($buyer);
		$shippingAddress = new \Iyzipay\Model\Address();
		$shippingAddress->setContactName($order->delivery_first_name);
		$shippingAddress->setCity($order->delivery_last_name);
		$shippingAddress->setCountry($order->delivery_country);
		$shippingAddress->setAddress($order->delivery_street1 . (($order->delivery_street2) ? ' ' . $order->delivery_street2 : '') . ', ' . $order->delivery_city);
		$shippingAddress->setZipCode($order->delivery_postal_code);
		$request->setShippingAddress($shippingAddress);
		$billingAddress = new \Iyzipay\Model\Address();
		$billingAddress->setContactName($order->billing_first_name . ' ' . $order->billing_last_name);
		$billingAddress->setCity($order->billing_zone);
		$billingAddress->setCountry($country[0]['country_name']);
		$billingAddress->setAddress($order->billing_street1 . (($order->billing_street2) ? ' ' . $order->billing_street2 : '') . ', ' . $order->billing_city);
		$billingAddress->setZipCode($order->billing_postal_code);
		$request->setBillingAddress($billingAddress);
		$basketItems = array();
		
		foreach($order->products as $id => $product) {
			$Item = new \Iyzipay\Model\BasketItem();
			$Item->setId($id);
			$Item->setName($product->title . ($product->qty>1) ? ' x ' . $product->qty : '');
			$Item->setCategory1('modules');
			$Item->setCategory2('');
			$Item->setItemType(\Iyzipay\Model\BasketItemType::VIRTUAL);
			$Item->setPrice($product->price*$product->qty);
			$basketItems[] = $Item;
		}
		
		$request->setBasketItems($basketItems);
		# make request
		$checkoutFormInitialize = \Iyzipay\Model\CheckoutFormInitialize::create($request, $this->_options);
		# print result
		//print_r($checkoutFormInitialize);
		//return $checkoutFormInitialize;
        
         return array(
			'#markup' => '<div id="iyzipay-checkout-form" class=" ">' . $checkoutFormInitialize->getCheckoutFormContent() . '</div>',
		  );
		  
	  }
	  
	  
	  public function retrieve_checkout_form()
	  {
	  
		$request = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
		$request->setLocale(\Iyzipay\Model\Locale::TR);
		$request->setConversationId($_SESSION['conversationId']);
		$request->setToken($_POST['token']);

		# make request
		$checkoutForm = \Iyzipay\Model\CheckoutForm::retrieve($request, $this->_options);

		# print result
		return $checkoutForm;
		
	}
    
}
