<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);




function uc_iyzico_callback() {
	
	if(!isset($_POST) || !isset($_SESSION['cart_order'])) {
		drupal_goto('cart');
            die();
	}
	
	$order = uc_order_load($_SESSION['cart_order']);
	
	require_once('library/IyzipayBootstrap.php');
      
      IyzipayBootstrap::init();
      
      # create client configuration
        $options = new \Iyzipay\Options();
        $options->setApiKey(variable_get('iyzi_api_key'));
        $options->setSecretKey(variable_get('iyzi_secret_key'));
        $options->setBaseUrl("https://api.iyzipay.com/");
        
        
      require_once('library/uc_iyziformLib.php');
	
	  $Iyziform = new CheckoutFormIyzico($options);
	  $result = $Iyziform->retrieve_checkout_form();
	  
	  if ($result->getPaymentStatus() == "SUCCESS") {
		  echo "<pre>";
		  //echo $result->getPaymentId();
		  //print_r($result); exit();
	   $comment = t('Iyzico Conversation id: @conversationid ', array('@conversationid' => $_SESSION['conversationId']));
	   $comment .= t('Iyzico Payment id: @paymentid', array('@paymentid' => $result->getPaymentId()));
                uc_payment_enter($order->order_id, 'iyzico', $order->order_total, $order->uid, NULL, $comment);
                uc_cart_complete_sale($order);
                uc_order_comment_save($order->order_id, 0, t('Iyzico reported a payment of @amount @currency.', array('@amount' => uc_currency_format($order->order_total, FALSE), '@currency' => $order->currency)));
                $_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'] = TRUE;
                drupal_goto('cart/checkout/complete');
                
        } else {
            drupal_set_message(t('Sipariş Başarısız! <br> <strong>Hata:</strong> @errorcode, @errormessage',array(
					'@errorcode' => $result->getErrorCode(), '@errormessage' => $result->getErrorMessage(),
					)
				)
            );
            drupal_goto('cart');
            die();
        }
	  
	  
}
